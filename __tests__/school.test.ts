import { autorun, reaction } from "mobx";
import { School, Student, Teacher, Subject } from "../src/components/index";
import { Counter } from "../src/counter";

const school = new School();

const students: Student[] = [
	new Student("Good", "Boy", "Good.Boy@student.com", "2002-09-25", school),
	new Student("Average", "Studentus", "Average.Studentus@student.com", "1998-05-28", school),
	new Student("Fail", "Badlious", "Fail.Badlious@student.com", "1985-04-07", school),
];
students.map((s) => school.addStudent(s));
const teachers: Teacher[] = [
	new Teacher("Mathematical", "Genious", "Mathematical.Genious@teacher.com", "1978-04-12", school),
	new Teacher("Biological", "Wizzard", "Biological.Wizzard@teacher.com", "1987-12-30", school),
	new Teacher("Language", "Speakius", "Language.Speakius@teacher.com", "1986-07-02", school),
];
teachers.map((t) => school.addTeacher(t));

const subjects: Subject[] = [
	new Subject(school, "Math", teachers[0]),
	new Subject(school, "Biology"),
	new Subject(school, "English"),
];
subjects.map((s) => {
	school.addSubject(s);
	school.enrolToSubject(students[0], s); //students[0].enrolToSubject(s);
	school.enrolToSubject(students[2], s); //students[2].enrolToSubject(s);
});

school.enrolToSubject(students[1], subjects[0]); //students[1].enrolToSubject(subjects[0]);
school.enrolToSubject(students[1], subjects[1]); //students[1].enrolToSubject(subjects[1]);
// student[3] is not enrolled to English

school.assignTeacherToSubject(teachers[1], subjects[1]);
school.assignTeacherToSubject(teachers[2], subjects[2]);
// false assignment same teacher for the same subject
school.assignTeacherToSubject(teachers[2], subjects[2]);

// Grades are set that all students have at least 6 grades for each subject

// Set grades for Math
school.putMarkToStudent(subjects[0], students[0], 4, teachers[0]);
school.putMarkToStudent(subjects[0], students[0], 3, teachers[0]);
school.putMarkToStudent(subjects[0], students[0], 4, teachers[0]);
school.putMarkToStudent(subjects[0], students[0], 4, teachers[0]);
school.putMarkToStudent(subjects[0], students[0], 2, teachers[0]);
school.putMarkToStudent(subjects[0], students[0], 5, teachers[0]); //22

school.putMarkToStudent(subjects[0], students[1], 2, teachers[0]);
school.putMarkToStudent(subjects[0], students[1], 3, teachers[0]);
school.putMarkToStudent(subjects[0], students[1], 2, teachers[0]);
school.putMarkToStudent(subjects[0], students[1], 3, teachers[0]);
school.putMarkToStudent(subjects[0], students[1], 2, teachers[0]);
school.putMarkToStudent(subjects[0], students[1], 2, teachers[0]); //14

school.putMarkToStudent(subjects[0], students[2], 2, teachers[0]);
school.putMarkToStudent(subjects[0], students[2], 1, teachers[0]);
school.putMarkToStudent(subjects[0], students[2], 2, teachers[0]);
school.putMarkToStudent(subjects[0], students[2], 1, teachers[0]);
school.putMarkToStudent(subjects[0], students[2], 2, teachers[0]);
school.putMarkToStudent(subjects[0], students[2], 2, teachers[0]); //10

// Set grades for Biology

school.putMarkToStudent(subjects[1], students[0], 5, teachers[1]);
school.putMarkToStudent(subjects[1], students[0], 5, teachers[1]);
school.putMarkToStudent(subjects[1], students[0], 4, teachers[1]);
school.putMarkToStudent(subjects[1], students[0], 4, teachers[1]);
school.putMarkToStudent(subjects[1], students[0], 5, teachers[1]);
school.putMarkToStudent(subjects[1], students[0], 5, teachers[1]); //28

school.putMarkToStudent(subjects[1], students[1], 3, teachers[1]);
school.putMarkToStudent(subjects[1], students[1], 3, teachers[1]);
school.putMarkToStudent(subjects[1], students[1], 3, teachers[1]);
school.putMarkToStudent(subjects[1], students[1], 3, teachers[1]);
school.putMarkToStudent(subjects[1], students[1], 2, teachers[1]);
school.putMarkToStudent(subjects[1], students[1], 1, teachers[1]); //15

school.putMarkToStudent(subjects[1], students[2], 1, teachers[1]);
school.putMarkToStudent(subjects[1], students[2], 1, teachers[1]);
school.putMarkToStudent(subjects[1], students[2], 2, teachers[1]);
school.putMarkToStudent(subjects[1], students[2], 1, teachers[1]);
school.putMarkToStudent(subjects[1], students[2], 2, teachers[1]);
school.putMarkToStudent(subjects[1], students[2], 3, teachers[1]); //10

// Set grades for English

school.putMarkToStudent(subjects[2], students[0], 5, teachers[2]);
school.putMarkToStudent(subjects[2], students[0], 5, teachers[2]);
school.putMarkToStudent(subjects[2], students[0], 2, teachers[2]);
school.putMarkToStudent(subjects[2], students[0], 4, teachers[2]);
school.putMarkToStudent(subjects[2], students[0], 2, teachers[2]);
school.putMarkToStudent(subjects[2], students[0], 5, teachers[2]); //23

school.putMarkToStudent(subjects[2], students[1], 3, teachers[2]);
school.putMarkToStudent(subjects[2], students[1], 5, teachers[2]);
school.putMarkToStudent(subjects[2], students[1], 3, teachers[2]);
school.putMarkToStudent(subjects[2], students[1], 2, teachers[2]);
school.putMarkToStudent(subjects[2], students[1], 2, teachers[2]);
school.putMarkToStudent(subjects[2], students[1], 1, teachers[2]); //16

school.putMarkToStudent(subjects[2], students[2], 5, teachers[2]);
school.putMarkToStudent(subjects[2], students[2], 5, teachers[2]);
school.putMarkToStudent(subjects[2], students[2], 2, teachers[2]);
school.putMarkToStudent(subjects[2], students[2], 3, teachers[2]);
school.putMarkToStudent(subjects[2], students[2], 5, teachers[2]);
school.putMarkToStudent(subjects[2], students[2], 5, teachers[2]); //25

// Wrong teacher assign grades for English
school.putMarkToStudent(subjects[2], students[0], 5, teachers[1]);
school.putMarkToStudent(subjects[2], students[0], 5, teachers[1]);
school.putMarkToStudent(subjects[2], students[0], 2, teachers[1]);
school.putMarkToStudent(subjects[2], students[0], 4, teachers[1]);
school.putMarkToStudent(subjects[2], students[0], 2, teachers[1]);
school.putMarkToStudent(subjects[2], students[0], 5, teachers[1]);

school.putMarkToStudent(subjects[2], students[1], 3, teachers[1]);
school.putMarkToStudent(subjects[2], students[1], 5, teachers[1]);
school.putMarkToStudent(subjects[2], students[1], 3, teachers[1]);
school.putMarkToStudent(subjects[2], students[1], 2, teachers[1]);
school.putMarkToStudent(subjects[2], students[1], 2, teachers[1]);
school.putMarkToStudent(subjects[2], students[1], 1, teachers[1]);

school.putMarkToStudent(subjects[2], students[2], 5, teachers[1]);
school.putMarkToStudent(subjects[2], students[2], 5, teachers[1]);
school.putMarkToStudent(subjects[2], students[2], 2, teachers[1]);
school.putMarkToStudent(subjects[2], students[2], 3, teachers[1]);
school.putMarkToStudent(subjects[2], students[2], 5, teachers[1]);
school.putMarkToStudent(subjects[2], students[2], 5, teachers[1]);
students[0].passedAllExams();
subjects[0].isStudentPassedTheExam(students[0]);

describe("student test", () => {
	it("meanGrade functions", () => {
		expect(students[0].getMeanGrade()).toMatchInlineSnapshot(`4.055555555555555`);
		expect(students[1].getMeanGrade()).toMatchInlineSnapshot(`2.4166666666666665`);
		expect(students[2].getMeanGrade()).toMatchInlineSnapshot(`2.5`);
		expect(students[0].getMeanGrade(subjects[0])).toMatchInlineSnapshot(`3.6666666666666665`);
		expect(students[1].getMeanGrade(subjects[0])).toMatchInlineSnapshot(`2.3333333333333335`);
		expect(students[2].getMeanGrade(subjects[0])).toMatchInlineSnapshot(`1.6666666666666667`);
		expect(students[0].getMeanGrade(subjects[1])).toMatchInlineSnapshot(`4.666666666666667`);
		expect(students[1].passedAllExams()).toMatchInlineSnapshot(`false`);
		expect(students[2].passedAllExams()).toMatchInlineSnapshot(`false`);
	});

	it("getHighestGrade functions", () => {
		expect(students[0].getHighestGrade()).toMatchInlineSnapshot(`5`);
		expect(students[1].getHighestGrade()).toMatchInlineSnapshot(`3`);
		expect(students[2].getHighestGrade()).toMatchInlineSnapshot(`5`);
		expect(students[0].getHighestGrade(subjects[0])).toMatchInlineSnapshot(`5`);
		expect(students[1].getHighestGrade(subjects[0])).toMatchInlineSnapshot(`3`);
		expect(students[2].getHighestGrade(subjects[0])).toMatchInlineSnapshot(`2`);
	});

	it("getLowestGrade functions", () => {
		expect(students[0].getLowestGrade()).toMatchInlineSnapshot(`2`);
		expect(students[1].getLowestGrade()).toMatchInlineSnapshot(`1`);
		expect(students[2].getLowestGrade()).toMatchInlineSnapshot(`1`);
		expect(students[0].getLowestGrade(subjects[0])).toMatchInlineSnapshot(`2`);
		expect(students[1].getLowestGrade(subjects[0])).toMatchInlineSnapshot(`2`);
		expect(students[2].getLowestGrade(subjects[0])).toMatchInlineSnapshot(`1`);
	});

	it("get Grade Count functions", () => {
		expect(students[0].getGradeCount()).toMatchInlineSnapshot(`18`);
		expect(students[1].getGradeCount()).toMatchInlineSnapshot(`12`);
		expect(students[2].getGradeCount()).toMatchInlineSnapshot(`18`);
		expect(students[0].getGradeCount(subjects[0])).toMatchInlineSnapshot(`6`);
		expect(students[1].getGradeCount(subjects[0])).toMatchInlineSnapshot(`6`);
		expect(students[2].getGradeCount(subjects[0])).toMatchInlineSnapshot(`6`);
		expect(students[0].getGradeCount(subjects[1])).toMatchInlineSnapshot(`6`);
		expect(students[1].getGradeCount(subjects[1])).toMatchInlineSnapshot(`6`);
		expect(students[2].getGradeCount(subjects[1])).toMatchInlineSnapshot(`6`);
		expect(students[0].getGradeCount(subjects[2])).toMatchInlineSnapshot(`6`);
		expect(students[1].getGradeCount(subjects[2])).toMatchInlineSnapshot(`-1`);
		expect(students[2].getGradeCount(subjects[2])).toMatchInlineSnapshot(`6`);
	});
});

describe("teacher test", () => {
	it("Get Mean grade", () => {
		expect(teachers[0].getMeanGrade()).toMatchInlineSnapshot(`2.5555555555555554`);
		expect(teachers[1].getMeanGrade()).toMatchInlineSnapshot(`2.9444444444444446`);
		//  commented and fixed due to the error.
		// The grades that teacher[2] puts to students[1] should not  be added to the main school journal.
		// in branch sl-branch students[1] didn't get grades from subjects[2],
		//  but in the same time teachers[2] collected them  in his assignedGrades Map
		// expect(teachers[2].getMeanGrade()).toMatchInlineSnapshot(`3.5555555555555554`);
		expect(teachers[2].getMeanGrade()).toMatchInlineSnapshot(`4`);
		expect(teachers[0].getMeanGrade(subjects[0])).toMatchInlineSnapshot(`2.5555555555555554`);
		expect(teachers[1].getMeanGrade(subjects[1])).toMatchInlineSnapshot(`2.9444444444444446`);
		expect(teachers[2].getMeanGrade(subjects[2])).toMatchInlineSnapshot(`4`);
		//expect(teachers[2].getMeanGrade(subjects[2])).toMatchInlineSnapshot(`3.5555555555555554`);
		// these teachers are not using this subjects
		expect(teachers[0].getMeanGrade(subjects[1])).toMatchInlineSnapshot(`-1`);
		expect(teachers[0].getMeanGrade(subjects[2])).toMatchInlineSnapshot(`-1`);
	});

	it("Can put marks to student", () => {
		expect(students[0].getGradeCount(subjects[0])).toMatchInlineSnapshot(`6`);
		// adding new mark

		school.putMarkToStudent(subjects[0], students[0], 4, teachers[0]);

		expect(students[0].getGradeCount(subjects[0])).toMatchInlineSnapshot(`7`);
		// wrong teacher put a mark
		school.putMarkToStudent(subjects[0], students[0], 4, teachers[1]);

		expect(students[0].getGradeCount(subjects[0])).toMatchInlineSnapshot(`7`);
	});
});

describe("general school test", () => {
	it("Best and worst", () => {
		console.log(school.printStudent());
		console.log(school.printTeacher());
		console.log(school.printDiplomas());

		expect(school.getBestStudent().name).toMatchInlineSnapshot(`"Good"`);
		expect(school.getMeanestTeacher().name).toMatchInlineSnapshot(`"Mathematical"`);
		expect(school.allSubjectsHasTeachers()).toMatchInlineSnapshot(`true`);
		// adding new subject without a teacher
		school.addSubject(new Subject(school, "Codding"));
		expect(school.allSubjectsHasTeachers()).toMatchInlineSnapshot(`false`);
	});

	it("Can test a subject", () => {
		expect(subjects[0].isStudentPassedTheExam(students[0])).toMatchInlineSnapshot(`true`);
		expect(subjects[1].isStudentPassedTheExam(students[0])).toMatchInlineSnapshot(`true`);
		expect(subjects[2].isStudentPassedTheExam(students[0])).toMatchInlineSnapshot(`true`);

		expect(subjects[0].isStudentPassedTheExam(students[1])).toMatchInlineSnapshot(`false`);
		expect(subjects[1].isStudentPassedTheExam(students[1])).toMatchInlineSnapshot(`false`);
		expect(subjects[2].isStudentPassedTheExam(students[1])).toMatchInlineSnapshot(`false`);

		expect(subjects[0].isStudentPassedTheExam(students[2])).toMatchInlineSnapshot(`false`);
		expect(subjects[1].isStudentPassedTheExam(students[2])).toMatchInlineSnapshot(`false`);
		expect(subjects[2].isStudentPassedTheExam(students[2])).toMatchInlineSnapshot(`true`);
	});

	describe("rest of  tests", () => {
		it("should add student with subject to school", () => {
			const thirdStudent = new Student(
				"Longest",
				"Lastnamusus",
				"Longest.Lastnamusus@student.com",
				"1986-07-02",
				school
			);
			school.addStudent(thirdStudent, school.subjects);
			expect(school.students).toContain(thirdStudent);
		});
	});

	it("should add teacher with subject to school", () => {
		const anotherTeacher = new Teacher("Napoleon", "Bonapart", "Napoleon.Bonapart@teacher.com", "1986-07-02", school);
		school.addTeacher(anotherTeacher, school.subjects[3]);
		expect(school.teachers).toContain(anotherTeacher);
	});

	it("should add subject with teacher to school", () => {
		const anotherSubject = new Subject(school, "History", school.teachers[3]);
		school.addSubject(anotherSubject, school.teachers[3]);
		expect(school.subjects).toContain(anotherSubject);
	});

	it("should print stat for a teacher", () => {
		expect(school.printTeacher(teachers[1])).toMatchInlineSnapshot(`
		"Name: Biological
		Surname: Wizzard
		Overall Mean teacher grade: 2.9444444444444446
		Subject: Biology
		  Students:
		     _______________________________________________________________________  
		     |  Student        | Mean Grade | Min Grade | Max Grade | Exam passed  |
		     |Good Boy         | 4.667      |    4      |    5      |    Yes       |
		     |Average Studentus| 2.500      |    1      |    3      |     No       |
		     |Fail Badlious    | 1.667      |    1      |    3      |     No       |"
	`);
	});

	it("should print stat for a student", () => {
		expect(school.printStudent(students[1])).toMatchInlineSnapshot(`
		"Name: Average
		Surname: Studentus
		Subjects:
		     ______________________________________________________________________________________  
		     |Subject name | Teacher            | Mean Grade | Min Grade | Max Grade | Exam passed |       
		     |Math         |Mathematical Genious| 2.417      |    1      |    3      |    No       |
		     |Biology      |Biological Wizzard  | 2.417      |    1      |    3      |    No       |"
	`);
	});

	describe("MOBX", () => {
		it("should automatic add new student to the journal", async () => {
			let callCount = 0;

			autorun(() => {
				console.log("The number of students is : " + school.students.length);
				callCount++;
			});
			// reaction(
			// 	() => school.students,
			// 	() => {
			// 		console.log("reaction ");
			// 	}
			// );
			const studentMobx = new Student("MobX", "Training", "Mobx.Training@student.com", "1990-12-12", school);
			const studentMobxTwo = new Student("MobX", "TrainingTwo", "Mobx.Training@student.com", "1976-06-30", school);
			school.addStudent(studentMobx);
			school.addStudent(studentMobxTwo);

			await new Promise<void>((resolve, reject) => {
				setTimeout(resolve, 200);
			});
			expect(callCount).toEqual(3);
		}, 500);
	});

	it("should automatic add new teacher to the journal", async () => {
		let callCount = 0;

		autorun(() => {
			console.log("the number of teachers is : " + school.teachers.length);
			callCount++;
		});
		// reaction(
		// 	() => school.students,
		// 	() => {
		// 		console.log("reaction ");
		// 	}
		// );
		const teacherMobx = new Teacher("MobX", "Training", "Mobx.Training@student.com", "1989-05-09", school);
		const teacherMobxTwo = new Teacher("MobX", "TrainingTwo", "Mobx.Training@student.com", "1984-09-12", school);
		school.addTeacher(teacherMobx);
		school.addTeacher(teacherMobxTwo);

		await new Promise<void>((resolve, reject) => {
			setTimeout(resolve, 200);
		});
		expect(callCount).toEqual(3);
	}, 500);

	it("should automatic add new subject to the journal", async () => {
		let callCount = 0;

		autorun(() => {
			console.log("the number of subjects is : " + school.subjects.length);
			callCount++;
		});
		// reaction(
		// 	() => school.students,
		// 	() => {
		// 		console.log("reaction ");
		// 	}
		// );
		const subjectMobx = new Subject(school, "History");
		const subjectMobxTwo = new Subject(school, "Chemistry");
		school.addSubject(subjectMobx);
		school.addSubject(subjectMobxTwo);

		await new Promise<void>((resolve, reject) => {
			setTimeout(resolve, 200);
		});
		expect(callCount).toEqual(3);
	}, 500);

	it("should add grades to a main school journal", async () => {
		let callCount = 0;

		autorun(() => {
			console.log("the grade was added this many times : " + school.journal.length);
			callCount++;
		});

		school.putMarkToStudent(subjects[0], students[0], 4, teachers[0]);
		school.putMarkToStudent(subjects[0], students[0], 4, teachers[0]);

		await new Promise<void>((resolve, reject) => {
			setTimeout(resolve, 200);
		});
		expect(callCount).toEqual(3);
	}, 500);

	it("should number of teachers in a teacher-subject journal", async () => {
		let callCount = 0;

		autorun(() => {
			console.log("the number of teachers in a teacher-subject journal : " + school.teacherSubjectJournal.length);
			callCount++;
		});

		school.assignTeacherToSubject(teachers[1], subjects[0]);
		school.assignTeacherToSubject(teachers[1], subjects[2]);

		await new Promise<void>((resolve, reject) => {
			setTimeout(resolve, 200);
		});
		expect(callCount).toEqual(3);
	}, 500);

	it("should number of students in a student-subject journal", async () => {
		let callCount = 0;

		autorun(() => {
			console.log("the number of students in a student-subject journal : " + school.enrollmentJournal.length);
			callCount++;
		});

		school.enrolToSubject(students[1], subjects[2]);

		await new Promise<void>((resolve, reject) => {
			setTimeout(resolve, 200);
		});
		expect(callCount).toEqual(2);
	}, 500);
});

// describe("counter test", () => {
// 	it("should add numbers", async () => {
// 		const newCounter = new Counter();

// 		autorun(() => {
// 			console.log("Number of items : " + newCounter.count);
// 		});
// 		newCounter.increment();
// 		newCounter.increment();
// 		await new Promise<void>((resolve, reject) => {
// 			setTimeout(resolve, 200);
// 		});
// 	}, 500);
// });
