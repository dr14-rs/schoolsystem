
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';// YourComponent.stories.ts|tsx


import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/popover2/lib/css/blueprint-popover2.css";
import "@blueprintjs/table/lib/css/table.css";
import "@blueprintjs/select/lib/css/blueprint-select.css";


import { SchoolContextProvider } from '../context/SchoolContext';
import { School, Student, Subject, Teacher } from '../components';
import Journal from '../components/Journal';



export const SchoolStory: React.FunctionComponent = () => {
    const school = new School();




    school.addSubject(new Subject(school, "Math", 3.3, 5,)),
        school.addSubject(new Subject(school, "Biology", 3.8, 5)),
        school.addSubject(new Subject(school, "English", 3.6, 6))

    school.addTeacher(new Teacher("Mathematical", "Genious", "Mathematical.Genious@teacher.com", "1978-04-12", school),)
    school.addTeacher(new Teacher("Biological", "Wizzard", "Biological.Wizzard@teacher.com", "1987-12-30", school),)
    school.addTeacher(new Teacher("Language", "Speakius", "Language.Speakius@teacher.com", "1986-07-02", school),)

    school.addStudent(new Student("Good", "Boy", "Good.Boy@student.com", "2002-09-25", school)),
        school.addStudent(new Student("Average", "Studentus", "Average.Studentus@student.com", "1998-05-28", school)),
        school.addStudent(new Student("Fail", "Badlious", "Fail.Badlious@student.com", "1985-04-07", school))



    school.subjects.map((s) => {
        school.enrolToSubject(school.students[0], s);
        school.enrolToSubject(school.students[1], s);
        school.enrolToSubject(school.students[2], s);
    });


    school.assignTeacherToSubject(school.teachers[0], school.subjects[0]);
    school.assignTeacherToSubject(school.teachers[1], school.subjects[1]);
    school.assignTeacherToSubject(school.teachers[2], school.subjects[2]);




    school.putMarkToStudent(school.subjects[0], school.students[0], 4, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[0], 3, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[0], 4, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[0], 4, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[0], 2, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[0], 5, school.teachers[0]); //22

    school.putMarkToStudent(school.subjects[0], school.students[1], 2, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[1], 3, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[1], 2, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[1], 3, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[1], 2, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[1], 2, school.teachers[0]); //14

    school.putMarkToStudent(school.subjects[0], school.students[2], 2, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[2], 1, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[2], 2, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[2], 1, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[2], 2, school.teachers[0]);
    school.putMarkToStudent(school.subjects[0], school.students[2], 2, school.teachers[0]); //10

    // Set grades for Biology

    school.putMarkToStudent(school.subjects[1], school.students[0], 5, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[0], 5, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[0], 4, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[0], 4, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[0], 5, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[0], 5, school.teachers[1]); //28

    school.putMarkToStudent(school.subjects[1], school.students[1], 3, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[1], 3, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[1], 3, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[1], 3, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[1], 2, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[1], 1, school.teachers[1]); //15

    school.putMarkToStudent(school.subjects[1], school.students[2], 1, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[2], 1, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[2], 2, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[2], 1, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[2], 2, school.teachers[1]);
    school.putMarkToStudent(school.subjects[1], school.students[2], 3, school.teachers[1]); //10

    // Set grades for English

    school.putMarkToStudent(school.subjects[2], school.students[0], 5, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[0], 5, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[0], 2, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[0], 4, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[0], 2, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[0], 5, school.teachers[2]); //23

    school.putMarkToStudent(school.subjects[2], school.students[1], 3, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[1], 5, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[1], 3, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[1], 2, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[1], 2, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[1], 1, school.teachers[2]); //16

    school.putMarkToStudent(school.subjects[2], school.students[2], 5, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[2], 5, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[2], 2, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[2], 3, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[2], 5, school.teachers[2]);
    school.putMarkToStudent(school.subjects[2], school.students[2], 5, school.teachers[2]); //25

    // Wrong teacher assign grades for English
    school.putMarkToStudent(school.subjects[2], school.students[0], 5, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[0], 5, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[0], 2, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[0], 4, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[0], 2, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[0], 5, school.teachers[1]);

    school.putMarkToStudent(school.subjects[2], school.students[1], 3, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[1], 5, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[1], 3, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[1], 2, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[1], 2, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[1], 1, school.teachers[1]);

    school.putMarkToStudent(school.subjects[2], school.students[2], 5, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[2], 5, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[2], 2, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[2], 3, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[2], 5, school.teachers[1]);
    school.putMarkToStudent(school.subjects[2], school.students[2], 5, school.teachers[1]);

    // console.log(school.journal.length, "journal length"
    // )
    // console.log(school.subjects, "subjects"
    // )
    // console.log(school.journal, "journal")
    // console.log(school.gradesForStudent(school.students[0]), "grades for student")
    // console.log(school.subjectForTeacher(school.teachers[0]), ", subject for teacher 0")
    school.printDiplomas







    return (<div>
        <SchoolContextProvider school={school}>
            <Journal />
        </SchoolContextProvider>
    </div>
    )
}
export default {
    title: "SchoolStory",
    component: SchoolStory
} as ComponentMeta<typeof SchoolStory>;

const Template: ComponentStory<typeof SchoolStory> = (args) => <Journal {...args} />

export const FirstStory = Template.bind({})
FirstStory.args = {

}