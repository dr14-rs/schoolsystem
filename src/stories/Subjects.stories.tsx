
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';// YourComponent.stories.ts|tsx


import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/popover2/lib/css/blueprint-popover2.css";
import "@blueprintjs/table/lib/css/table.css";
import "@blueprintjs/select/lib/css/blueprint-select.css";


import { SchoolContextProvider } from '../context/SchoolContext';
import { School, Subject, Teacher } from '../components';
import Students from '../components/StudentsTable/Students';
import Subjects from '../components/SubjectsTable/Subjects';



export const SubjectStory: React.FunctionComponent = () => {

    const school = new School();
    const teachers = school.teachers
    const subjects = school.subjects
    const students = school.students

    school.addSubject(new Subject(school, "History", 3.3, 5)),
        school.addSubject(new Subject(school, "Math", 3.8, 5)),
        school.addSubject(new Subject(school, "English", 3.6, 6))

    school.addTeacher(new Teacher("Mathematical", "Genious", "Mathematical.Genious@teacher.com", "1978-04-12", school),)
    school.addTeacher(new Teacher("Biological", "Wizzard", "Biological.Wizzard@teacher.com", "1987-12-30", school),)
    school.addTeacher(new Teacher("Language", "Speakius", "Language.Speakius@teacher.com", "1986-07-02", school),)


    school.enrollmentJournal = [{ student: "Good", subject: "Math" }]

    console.log(school.teachers, school.subjects)


    school.assignTeacherToSubject(teachers[0], subjects[0])

    console.log(school.teacherSubjectJournal.length)
    return (<div>
        <SchoolContextProvider school={school}>
            <Subjects />
        </SchoolContextProvider>
    </div>
    )
}
export default {
    title: "SubjectStory",
    component: SubjectStory
} as ComponentMeta<typeof SubjectStory>;

const Template: ComponentStory<typeof SubjectStory> = (args) => <Students {...args} />

export const FirstStory = Template.bind({})
FirstStory.args = {

}