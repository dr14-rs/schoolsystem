
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';// YourComponent.stories.ts|tsx


import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/popover2/lib/css/blueprint-popover2.css";
import "@blueprintjs/table/lib/css/table.css";


import { SchoolContextProvider } from '../context/SchoolContext';
import { School, Student } from '../components';
import Students from '../components/StudentsTable/Students';



export const StudentStory: React.FunctionComponent = () => {
    const school = new School();
    school.addStudent(new Student("Good", "Boy", "Good.Boy@student.com", "2002-09-25", school)),
        school.addStudent(new Student("Average", "Studentus", "Average.Studentus@student.com", "1998-05-28", school)),
        school.addStudent(new Student("Fail", "Badlious", "Fail.Badlious@student.com", "1985-04-07", school))

    return (<div>
        <SchoolContextProvider school={school}>
            <Students />
        </SchoolContextProvider>
    </div>
    )
}
export default {
    title: "StudentStory",
    component: StudentStory
} as ComponentMeta<typeof StudentStory>;

const Template: ComponentStory<typeof StudentStory> = (args) => <Students {...args} />

export const FirstStory = Template.bind({})
FirstStory.args = {

}