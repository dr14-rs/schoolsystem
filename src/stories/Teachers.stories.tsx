
import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';// YourComponent.stories.ts|tsx


import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";
import "@blueprintjs/popover2/lib/css/blueprint-popover2.css";
import "@blueprintjs/table/lib/css/table.css";


import Teachers from '../components/TeachersTable/Teachers';
import { SchoolContextProvider } from '../context/SchoolContext';
import { School, Teacher } from '../components';



export const TeacherStory: React.FunctionComponent = () => {
    const school = new School();
    school.addTeacher(new Teacher("Mathematical", "Genious", "Mathematical.Genious@teacher.com", "1978-04-12", school),)
    school.addTeacher(new Teacher("Biological", "Wizzard", "Biological.Wizzard@teacher.com", "1987-12-30", school),)
    school.addTeacher(new Teacher("Language", "Speakius", "Language.Speakius@teacher.com", "1986-07-02", school),)


    return (<div>
        <SchoolContextProvider school={school}>
            <Teachers />
        </SchoolContextProvider>
    </div>
    )
}
export default {
    title: "TeacherStory",
    component: TeacherStory
} as ComponentMeta<typeof TeacherStory>;

const Template: ComponentStory<typeof TeacherStory> = (args) => <Teachers {...args} />

export const FirstStory = Template.bind({})
FirstStory.args = {

}