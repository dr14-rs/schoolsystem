import { computed } from "mobx";
import { Person } from "./person";
import { School } from "./school";
import { Subject } from "./subject";

export type IGrade = 1 | 2 | 3 | 4 | 5;

export class Student extends Person {
	constructor(name: string, surname: string, email: string, dateOfBirth: string, school: School) {
		super(name, surname, email, dateOfBirth, school);
	}
	@computed get age(): number {
		var today = new Date();
		var birthDate = new Date(this.dateOfBirth);
		var age = today.getFullYear() - birthDate.getFullYear();
		var months = today.getMonth() - birthDate.getMonth();
		if (months < 0 || (months === 0 && today.getDate() < birthDate.getDate())) {
			age = age - 1;
		}
		return age;
	}
	@computed get enrolledSubjects(): Subject[] {
		return this.school.studentEnrolledSubjects(this);
	}

	@computed get journal(): Map<string, IGrade[]> {
		return this.school.gradesForStudent(this);
	}

	/**
	 * Get lowest value for grades
	 *    if subject is not  defined then return lowest value of all grades
	 * @param subject
	 */
	getLowestGrade(subject?: Subject): IGrade {
		if (subject) {
			const marks = this.journal.get(subject.name) || [];
			return Math.min(...marks) as IGrade;
		} else {
			const allMarks = [...this.journal.values()].flat();
			return Math.min(...allMarks) as IGrade;
		}
	}

	/**
	 * Get highest value for grades
	 *    if subject is not  defined then return highest value of all grades
	 * @param subject
	 */
	getHighestGrade(subject?: Subject): IGrade {
		if (subject) {
			const marks = this.journal.get(subject.name) || [];
			return Math.max(...marks) as IGrade;
		} else {
			const allMarks = [...this.journal.values()].flat();
			return Math.max(...allMarks) as IGrade;
		}
	}

	/**
	 * Get mean value for grades
	 *    if subject is not  defined then return mean value of all grades
	 * @param subject
	 */

	getMeanGrade(subject?: Subject): number {
		if (subject) {
			const marks = this.journal.get(subject.name) || [];
			const result = marks.reduce((acc, cur) => acc + cur, 0) / marks.length;
			return Number(result.toFixed(2));
		} else {
			const allMarks = [...this.journal.values()].flat();
			const result = allMarks.reduce((acc, cur) => acc + cur, 0) / allMarks.length;
			return Number(result.toFixed(2));
		}
	}

	/**
	 * Get counts of grades
	 *    if subject is not  defined then return count of all grades
	 * @param subject
	 */
	getGradeCount(subject?: Subject): number {
		if (subject) {
			if (!this.enrolledSubjects.includes(subject)) {
				return -1;
			}
			const marks = this.journal.get(subject.name) || [];
			return marks.length;
		} else {
			const allMarks = [...this.journal.values()].flat();
			return allMarks.length;
		}
	}

	/**
	 * Check if student passed all his exams (refer to subject class to see the conditions)
	 * @returns
	 */
	passedAllExams(): boolean {
		return this.enrolledSubjects.filter((subj) => !this.examPassed(subj)).length === 0;
	}

	/**
	 * Check if user passed the exam for subject
	 * @param subject
	 * @returns
	 */
	examPassed(subject: Subject): boolean {
		return subject.isStudentPassedTheExam(this);
	}

	/* should return a string in the form.
	 *  ____________________________________________________________________________________________________________
	 *  |  Name: xxxx                                                                                              |
	 *  |  Surname: xxx                                                                                            |
	 *  |  Subjects:                                                                                               |
	 *  |     ______________________________________________________________________________________________       |
	 *  |     |Subject name    | Teacher    |  Mean Grade   |  Min Grade  |   Max Grade    | Exam passed   |       |
	 *  |     |   XXXX         | Name SrName|    X          |     X       |      X         | "Yes" or "no" |       |
	 *  |     |    ....                                                                                    |       |
	 *  |     ----------------------------------------------------------------------------------------------       |
	 *  ------------------------------------------------------------------------------------------------------------
	 */
	printStudentStats(): string {
		const subjectColumnRequiredLength = Math.max(...this.enrolledSubjects.map((subj) => subj.name.length));

		const subjectColumn = `|Subject name`.padEnd(subjectColumnRequiredLength, " ");

		const teacherColumnRequiredLength = Math.max(
			...this.enrolledSubjects.map((subj) => (subj.teacher?.name + " " + subj.teacher?.surname).length)
		);
		const teacherColumn =
			teacherColumnRequiredLength < `| Teacher `.length
				? `| Teacher `
				: `| Teacher `.padEnd(teacherColumnRequiredLength, " ");

		const msg: string[] = [
			`Name: ${this.name}`,
			`Surname: ${this.surname}`,
			`Subjects:`,
			`     ______________________________________________________________________________________  `,
			`     ${subjectColumn} ${teacherColumn} | Mean Grade | Min Grade | Max Grade | Exam passed |       `,
		];

		for (const s of this.enrolledSubjects) {
			const meanGradeLength = " Mean Grade ".length;
			const lowestGradeLength = " Min Grade ".length;
			const highestGradeLength = " Max Grade ".length;
			const passExamLength = " Exam passed ".length;

			const name = s.name.padEnd(subjectColumn.length, " ");
			const teacherName = (s.teacher?.name + " " + s.teacher?.surname).padEnd(teacherColumn.length, " ");
			const subjectMeanGrade = this.getMeanGrade()
				.toFixed(3)
				.toString()
				.padStart(meanGradeLength / 2, " ")
				.padEnd(meanGradeLength, " ");
			const subjectLowestGrade = this.getLowestGrade()
				.toString()
				.padStart(lowestGradeLength / 2, " ")
				.padEnd(lowestGradeLength, " ");

			const subjectHighestGrade = this.getHighestGrade()
				.toString()
				.padStart(highestGradeLength / 2, " ")
				.padEnd(highestGradeLength, " ");

			const subjectPassed = s.isStudentPassedTheExam(this)
				? "Yes".padStart(passExamLength / 2, " ").padEnd(passExamLength, " ")
				: "No".padStart(passExamLength / 2, " ").padEnd(passExamLength, " ");

			msg.push(
				`     |${name}|${teacherName}|${subjectMeanGrade}|${subjectLowestGrade}|${subjectHighestGrade}|${subjectPassed}|`
			);
		}
		return msg.join("\n");
	}
}
