import { School } from "./school";

export interface IPerson {
	name: string;
	surname: string;
	email: string;
	dateOfBirth: string;
}
export class Person implements IPerson {
	name: string;
	surname: string;
	email: string;
	dateOfBirth: string;
	protected school: School;

	constructor(name: string, surname: string, email: string, dateOfBirth: string, school: School) {
		// Write the constructor
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.dateOfBirth = dateOfBirth;
		this.school = school;
	}
}
