import { action, observable } from "mobx";
import { IGrade, Student } from "./student";
import { Subject } from "./subject";
import { Teacher } from "./teacher";

interface IJournal {
	subject: Subject;
	teacher: Teacher;
	student: Student;
	grade: IGrade;
}

interface IEnrollment {
	student: Student;
	subject: Subject;
}

interface ITeacherSubject {
	teacher: Teacher;
	subject: Subject;
}
export class School {
	@observable subjects: Subject[];
	@observable teachers: Teacher[];
	@observable students: Student[];
	@observable journal: IJournal[];
	@observable enrollmentJournal: IEnrollment[]; // student=>subject
	@observable teacherSubjectJournal: ITeacherSubject[]; // teacher=>subject

	constructor(subjects?: Subject[], teachers?: Teacher[], students?: Student[]) {
		// implement the constructor
		this.subjects = subjects || [];
		this.teachers = teachers || [];
		this.students = students || [];
		this.journal = [];
		this.enrollmentJournal = [];
		this.teacherSubjectJournal = [];
	}

	/**
	 * Should add a subject to the school
	 * If teacher is defined then assign a subject to a teacher right away
	 * @param subject
	 * @param teacher
	 */
	@action.bound
	addSubject(subject: Subject, teacher?: Teacher): void {
		if (!this.subjects.filter((filteredSubject) => filteredSubject.name === subject.name)[0]) {
			this.subjects = [...this.subjects, subject];
		} else {
			throw new Error("The subject defined");
		}
		if (teacher) {
			this.assignTeacherToSubject(teacher, subject);
		}
	}

	/**
	 * Should add a teacher to the school
	 * If subject is defined then assign a subject to a teacher right away
	 * @param subject
	 * @param teacher
	 */
	@action.bound
	addTeacher(teacher: Teacher, subject?: Subject): void {
		if (!this.teachers.filter((filteredTeacher) => filteredTeacher.email === teacher.email)[0]) {
			this.teachers = [...this.teachers, teacher];
		} else {
			throw new Error("The teacher defined");
		}
		if (subject) {
			this.assignTeacherToSubject(teacher, subject);
		}
	}

	/**
	 * Should add student to a school do not forget to test if student is already there
	 * if subjects are defined enroll student to these subjects right away (do not forget to check if subjects are already in school)
	 * @param student
	 * @param subjects
	 */
	@action.bound
	addStudent(student: Student, subjects?: Array<Subject>): void {
		if (!this.students.filter((filteredStudent) => filteredStudent.email === student.email)[0]) {
			this.students = [...this.students, student];
		} else {
			throw new Error("The student defined");
		}
		if (subjects) {
			subjects.forEach((subject) => this.enrolToSubject(student, subject));
		}
	}

	teacherForSubject(subject: Subject): Teacher | undefined {
		const record = this.teacherSubjectJournal.find((record) => record.subject === subject);
		if (record) {
			return this.teachers.find((teacher) => teacher === record.teacher);
		}
	}
	subjectForTeacher(teacher: Teacher): Subject | undefined {
		const record = this.teacherSubjectJournal.find((record) => record.teacher === teacher);
		if (record) {
			return this.subjects.find((subject) => subject.name === record.subject.name);
		}
	}

	/**
	 * Should assign a teacher to a subject and subject to a teacher
	 * Do not forget to add teacher or subject to the school if they are not there yet
	 *
	 * @param teacher
	 * @param subject
	 */
	gradesForTeacher(teacher: Teacher): Map<string, IGrade[]> {
		const subjectGradesMap = new Map();
		for (const record of this.journal) {
			if (record.teacher === teacher) {
				if (subjectGradesMap.has(record.subject)) {
					subjectGradesMap.get(record.subject).push(record.grade);
				} else {
					subjectGradesMap.set(record.subject, [record.grade]);
				}
			}
		}
		return subjectGradesMap;
	}
	gradesForStudent(student: Student): Map<string, IGrade[]> {
		const subjectGradesMap = new Map();
		for (const record of this.journal) {
			if (record.student === student) {
				if (subjectGradesMap.has(record.subject.name)) {
					subjectGradesMap.get(record.subject.name).push(record.grade);
				} else {
					subjectGradesMap.set(record.subject.name, [record.grade]);
				}
			}
		}

		return subjectGradesMap;
	}

	teacherEnrolledSubjects(teacher: Teacher): Subject[] {
		const records = this.teacherSubjectJournal.filter((record) => record.teacher === teacher);
		const subjectNames = records.map(({ subject }) => subject);
		return this.subjects.filter((subject) => subjectNames.includes(subject));
	}

	studentEnrolledSubjects(student: Student): Subject[] {
		const records = this.enrollmentJournal.filter((record) => record.student === student);
		const subjectNames = records.map(({ subject }) => subject);
		return this.subjects.filter((subject) => subjectNames.includes(subject));
	}

	subjectEnrolledStudents(subject: Subject): Student[] {
		const records = this.enrollmentJournal.filter((record) => record.subject === subject);
		const subjectNames = records.map(({ student }) => student);
		return this.students.filter((student) => subjectNames.includes(student));
	}

	isTeacherEnrolledToSubject(teacher: Teacher, subject: Subject): boolean {
		if (this.teacherSubjectJournal.find((record) => record.teacher === teacher && record.subject === subject)) {
			return true;
		} else {
			return false;
		}
	}

	@action.bound
	assignTeacherToSubject(teacher: Teacher, subject: Subject): void {
		// if (!this.subjects.includes(subject) && this.subjects.length < 1) {//
		//   this.subjects.push(subject);
		//   subject.assignNewTeacher(this);
		// }
		const record = this.teacherSubjectJournal.find(
			(record) => record.teacher === teacher && record.subject === subject
		);
		if (!record) {
			this.teacherSubjectJournal.push({
				teacher: teacher,
				subject: subject,
			});
		}
	}

	/**
	 * Enroll student to the subject if he has not been yet enrolled
	 * @param subject
	 */
	@action.bound
	enrolToSubject(student: Student, subject: Subject): void {
		const record = this.enrollmentJournal.find((record) => record.student === student && record.subject === subject);
		if (!record) {
			this.enrollmentJournal.push({
				student: student,
				subject: subject,
			});
		}
	}

	/**
	 * add a grade for a  subject
	 * @param subject
	 * @param mark
	 */
	@action.bound
	putMarkToStudent(subject: Subject, student: Student, grade: IGrade, teacher: Teacher): void {
		const isStudentEnrolledToSubject = this.enrollmentJournal.find(
			(record) => record.student === student && record.subject === subject
		);

		const isTeacherEnrolledToSubject = this.teacherSubjectJournal.find(
			(record) => record.teacher === teacher && record.subject === subject
		);

		if (isStudentEnrolledToSubject && isTeacherEnrolledToSubject) {
			this.journal.push({
				subject: subject,
				student: student,
				teacher: teacher,
				grade,
			});
		}
	}

	getBestStudent(subject?: Subject): Student {
		const [bestStudent] = this.students
			.map((student) => ({
				student,
				meanGrade: student.getMeanGrade(subject),
			}))
			.sort((a, b) => {
				if (a.meanGrade > b.meanGrade) {
					return -1;
				}
				if (a.meanGrade < b.meanGrade) {
					return 1;
				}
				// a must be equal to b
				return 0;
			});
		return bestStudent.student;
	}

	/**
	 * Get a meanest teacher in the school (the teacher who put lowest mean grade)
	 * if subject is defined then  return a teacher whit lowest Mean grade for a subject
	 * if not defined then return a teacher with lowest mean grade across all his subjects
	 * @param subject
	 * @returns
	 */
	getMeanestTeacher(subject?: Subject): Teacher {
		const [meanestTeacher] = this.teachers
			.map((teacher) => ({
				teacher,
				teacherMeanGrade: teacher.getMeanGrade(subject),
			}))
			.sort((a, b) => {
				if (a.teacherMeanGrade < b.teacherMeanGrade) {
					return -1;
				}
				if (b.teacherMeanGrade > b.teacherMeanGrade) {
					return 1;
				}
				// a must be equal to b
				return 0;
			});
		return meanestTeacher.teacher;
	}

	/**
	 * Test if all subjects has teachers
	 * @returns true if yes and false if no
	 */
	allSubjectsHasTeachers(): boolean {
		//map method over subj
		return this.subjects.filter((subj) => !subj.teacher).length === 0;
	}

	/**
	 * Should return a string of teachers stat for a teacher or for all teacher if teacher is not defined
	 *  Name Surname
	 *  Overall Mean grade: XXX
	 *  Subject Name meanGrade XXX
	 *  */
	printTeacher(teacher?: Teacher): string {
		if (teacher) {
			return teacher.printTeacherStat();
		} else {
			return this.teachers.map((teacher) => teacher.printTeacherStat()).join("\n");
		}
	}

	/**
	 * Should return a string of students stat for a student or for all student if teacher is not defined
	 *  Name Surname
	 *  Overall Mean grade: XXX
	 *  Subject Name meanGrade XXX
	 *  */
	printStudent(student?: Student): string {
		if (student) {
			return student?.printStudentStats();
		} else {
			return this.students.map((student) => student.printStudentStats()).join("\n");
		}
	}

	/**
	 * print stats for all students who passed their exams
	 */
	printDiplomas(): string {
		return this.students
			.filter((student) => student.passedAllExams())
			.map((student) => student.printStudentStats())
			.join("");
	}
}
