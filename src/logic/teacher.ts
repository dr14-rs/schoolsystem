import { Person } from "./person";
import { Subject } from "./subject";
import { IGrade, Student } from "./student";
import { School } from "./school";
import { computed } from "mobx";

export class Teacher extends Person {
	constructor(name: string, surname: string, email: string, dateOfBirth: string, school: School) {
		// Implement the constructor
		super(name, surname, email, dateOfBirth, school);
	}

	@computed get age(): number {
		var today = new Date();
		var birthDate = new Date(this.dateOfBirth);
		var age = today.getFullYear() - birthDate.getFullYear();
		var months = today.getMonth() - birthDate.getMonth();
		if (months < 0 || (months === 0 && today.getDate() < birthDate.getDate())) {
			age = age - 1;
		}
		return age;
	}
	@computed get subjects(): Subject[] {
		return this.school.teacherEnrolledSubjects(this);
	}

	@computed get assignedGrades(): Map<string, IGrade[]> {
		return this.school.gradesForTeacher(this);
	}

	/**
	 * Calculate mean value for grades for subject if defined or for all subjects
	 * return -1 if teacher is not teaching this subject
	 * @param subject
	 */
	getMeanGrade(subject?: Subject): number {
		if (subject) {
			if (this.school.isTeacherEnrolledToSubject(this, subject) === false) {
				return -1;
			}

			const grades = this.assignedGrades.get(subject.name) || [];
			return grades.reduce((acc, cur) => acc + cur, 0) / grades.length;
		} else {
			const grades = [...this.assignedGrades.values()].flat();
			return grades.reduce((acc, cur) => acc + cur, 0) / grades.length;
		}
	}

	/* should return a string in the exact form (Respect table view and column sizes)    
  ___________________________________________________________________________________________
  |  Name: xxxx                                                                             |
  |  Surname: xxx  
  |  Overall Mean teacher grade: XXX                                                                         |
  |  Subject: XXXX                                                                          |
  |    Students:                                                                            |
  |       _____________________________________________________________________________     |
  |       | Student    |  Mean Grade   |  Min Grade  |   Max Grade    | Exam passed   |     |
  |       | Name SrName|    X          |     [X, Y]  |     [X, Y]     |["Yes" or "no"]|     |
  |       |                                                                           |     |
  |       -----------------------------------------------------------------------------     |
  ------------------------------------------------------------------------------------------- 
*/
	printTeacherStat(): string {
		const msg: string[] = [
			`Name: ${this.name}`,
			`Surname: ${this.surname}`,
			`Overall Mean teacher grade: ${this.getMeanGrade()}`,
		];

		for (const subject of this.subjects) {
			msg.push(`Subject: ${subject.name}`);

			msg.push(`  Students:`);

			msg.push(`     _______________________________________________________________________  `);

			const studentColumnRequiredLength = Math.max(
				...subject.enrolledStudents.map((s) => (s.name + " " + s.surname).length)
			);
			const studentColumn = "|  Student".padEnd(studentColumnRequiredLength, " ");
			const meanGradeLength = " Mean Grade ".length;
			const lowestGradeLength = " Min Grade ".length;
			const highestGradeLength = " Max Grade ".length;
			const passExamLength = " Exam passed  ".length;

			msg.push(`     ${studentColumn} | Mean Grade | Min Grade | Max Grade | Exam passed  |`);

			for (const s of subject.enrolledStudents) {
				const nameAndSurname = (s.name + " " + s.surname).padEnd(studentColumnRequiredLength, " ");

				const subjectMeanGrade = s
					.getMeanGrade(subject)
					.toFixed(3)
					.toString()
					.padStart(meanGradeLength / 2, " ")
					.padEnd(meanGradeLength, " ");
				const subjectLowestGrade = s
					.getLowestGrade(subject)
					.toString()
					.padStart(lowestGradeLength / 2, " ")
					.padEnd(lowestGradeLength, " ");

				const subjectHighestGrade = s
					.getHighestGrade(subject)
					.toString()
					.padStart(highestGradeLength / 2, " ")
					.padEnd(highestGradeLength, " ");

				const subjectPassed = s.examPassed(subject)
					? "Yes".padStart(passExamLength / 2, " ").padEnd(passExamLength, " ")
					: "No".padStart(passExamLength / 2, " ").padEnd(passExamLength, " ");

				msg.push(
					`     |${nameAndSurname}|${subjectMeanGrade}|${subjectLowestGrade}|${subjectHighestGrade}|${subjectPassed}|`
				);
			}
		}

		return msg.join("\n");
	}
}
