import { Teacher } from "./teacher";
import { Student } from "./student";
import { School } from "./school";
import { action, computed } from "mobx";

export class Subject {
	name: string;
	meanGradeToPassExam: number;
	minAmountOfGradesToPassTheExam: number;
	school: School;
	constructor(
		school: School,
		name: string,
		meanGradeToPassExam: number,
		minAmountOfGradesToPassTheExam: number,
		teacher?: Teacher
	) {
		this.name = name;
		this.school = school;
		this.meanGradeToPassExam = meanGradeToPassExam;
		this.minAmountOfGradesToPassTheExam = minAmountOfGradesToPassTheExam;
	}

	@computed get teacher(): Teacher | undefined {
		return this.school.teacherForSubject(this);
	}

	@computed get enrolledStudents(): Student[] {
		return this.school.subjectEnrolledStudents(this);
	}

	/**
	 * check if a student passed the exam
	 * use meanGradeToPassExam and minAmountOfGradesToPassTheExam to evaluate
	 * Don t forget to check if student has been enrolled to the subject
	 * @param student
	 */
	isStudentPassedTheExam(student: Student): boolean {
		if (this.enrolledStudents.includes(student)) {
			return (
				this.minAmountOfGradesToPassTheExam <= student.getGradeCount(this) &&
				this.meanGradeToPassExam <= student.getMeanGrade(this)
			);
		} else {
			return false;
		}
	}
}
