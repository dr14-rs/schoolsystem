import { Tabs, Tab } from "@blueprintjs/core";
import { Cell, Column, Table2 } from "@blueprintjs/table";
import { observer } from "mobx-react";
import React, { useContext } from "react";
import { SchoolContext } from "../context/SchoolContext";
import Students from "./StudentsTable/Students";
import Subjects from "./SubjectsTable/Subjects";
import Teachers from "./TeachersTable/Teachers";






const Journal: React.FunctionComponent = observer(() => {
    const school = useContext(SchoolContext)








    return (
        <div>

            <Tabs id="TabsExample"
                large={true}
                vertical={true}>
                <Tab id="ng" title="Teachers" panel={<Teachers />} />
                <Tab id="mb" title="Students" panel={<Students />} panelClassName="ember-panel" />
                <Tab id="rx" title="Subjects" panel={<Subjects />} />
            </Tabs>




            <h1>Teachers :</h1>
            <Table2 numRows={school.teachers.length}

            >
                <Column
                    key="name"
                    name="Name"
                    cellRenderer={index => {
                        let record = school.teacherSubjectJournal[index]
                        let teacherNameRender = `${record.teacher.name}`;
                        return <Cell>{teacherNameRender}</Cell>
                    }} />
                <Column
                    key="surname"
                    name="Surname"
                    cellRenderer={index => {
                        let record = school.teacherSubjectJournal[index]
                        let teacherSurnameRender = `${record.teacher.surname}`;
                        return <Cell>{teacherSurnameRender}</Cell>
                    }} />
                <Column
                    key="age"
                    name="Age"
                    cellRenderer={index => {
                        let record = school.teacherSubjectJournal[index]
                        let teacherAgeRender = `${record.teacher.age}`;
                        return <Cell>{teacherAgeRender}</Cell>
                    }} />
                <Column
                    key="email"
                    name="Email"
                    cellRenderer={index => {
                        let record = school.teacherSubjectJournal[index]
                        let teacherEmailRender = `${record.teacher.email}`;
                        return <Cell>{teacherEmailRender}</Cell>
                    }} />
                <Column
                    key="meanGrade"
                    name="Mean Grade "
                    cellRenderer={index => {
                        let record = school.teacherSubjectJournal[index]
                        let teacherSurnameRender = `${record.teacher.getMeanGrade()}`;
                        return <Cell>{teacherSurnameRender}</Cell>
                    }} />

            </Table2>

            <h1>Students:</h1>
            <Table2 numRows={school.students.length}

            >
                <Column
                    key="name"
                    name="Name"
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let studentNameRender = `${record.student.name}`;
                        return <Cell>{studentNameRender}</Cell>
                    }} />
                <Column
                    key="surname"
                    name="Surname"
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let teacherSurnameRender = `${record.student.surname}`;
                        return <Cell>{teacherSurnameRender}</Cell>
                    }} />
                <Column
                    key="age"
                    name="Age"
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let studentAgeRender = `${record.student.age}`;
                        return <Cell>{studentAgeRender}</Cell>
                    }} />
                <Column
                    key="email"
                    name="Email"
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let studentEmailRender = `${record.student.email}`;
                        return <Cell>{studentEmailRender}</Cell>
                    }} />
                <Column
                    key="meanGrade"
                    name="Mean Grade "
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let teacherSurnameRender = `${record.student.getMeanGrade()}`;
                        return <Cell>{teacherSurnameRender}</Cell>
                    }} />

            </Table2>






        </div>













    )

})



export default Journal