import React from "react";
import {
    Alignment,
    AnchorButton,
    Classes,
    Navbar,
    NavbarGroup,
    NavbarHeading,
    NavbarDivider
} from "@blueprintjs/core";

export interface NavigationProps { }

export const SchoolNavbar: React.FC<NavigationProps> = () => {
    return (
        <Navbar className={Classes.DARK}>
            <NavbarGroup align={Alignment.LEFT}>
                <NavbarHeading>School app</NavbarHeading>
                <NavbarDivider />
                <AnchorButton
                    href=""
                    text="Contact Us"
                    target="_blank"
                    minimal
                    rightIcon="share"
                />
                <AnchorButton
                    href=""
                    text="About"
                    target="_blank"
                    minimal
                    rightIcon="code"
                />
            </NavbarGroup>
        </Navbar>
    );
};

export default SchoolNavbar
