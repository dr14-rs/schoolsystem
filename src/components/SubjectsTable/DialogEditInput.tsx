import { Dialog, Classes, InputGroup, Button, Toaster, Toast } from "@blueprintjs/core"
import React, { useContext, useState } from "react"
import { SchoolContext } from "../../context/SchoolContext"
import { Subject } from "../../logic/subject";



interface IProps {
    setEditingSubject: (editingSubject: Subject | undefined) => void;
    editingSubject: Subject | undefined;

    error: string | undefined
    setError: (error: string | undefined) => void;

    isEditDialogOpen: boolean;
    setIsEditDialogOpen: (isEdiDialogOPen: boolean) => void

}

const DialogEditInput: React.FunctionComponent<IProps> = (props) => {
    const school = useContext(SchoolContext)
    const subjects = school.subjects


    const [editedDetails, setEditedDetails] = useState({
        name: props?.editingSubject?.name || "",
        meanGradeToPassExam: props.editingSubject?.meanGradeToPassExam || 0,
        minAmountOfGradesToPassTheExam: props.editingSubject?.minAmountOfGradesToPassTheExam || 0,

    })

    React.useEffect(() => {
        setEditedDetails({
            name: props?.editingSubject?.name || "",
            meanGradeToPassExam: props.editingSubject?.meanGradeToPassExam || 0,
            minAmountOfGradesToPassTheExam: props.editingSubject?.minAmountOfGradesToPassTheExam || 0,

        })
    }, [props.editingSubject])

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {

        const { name, value } = e.target
        console.log(name, value)
        setEditedDetails((prev) => {
            return { ...prev, [name]: value }
        })

    }
    const handleSubmit = (e: React.MouseEvent<HTMLElement>) => {
        const teacherToChange = school.subjects.find((teacher) => teacher.name === props?.editingSubject?.name &&
            teacher.meanGradeToPassExam === props?.editingSubject.meanGradeToPassExam &&
            teacher.minAmountOfGradesToPassTheExam === props.editingSubject.minAmountOfGradesToPassTheExam)!



        teacherToChange.name = editedDetails.name
        teacherToChange.meanGradeToPassExam = editedDetails.meanGradeToPassExam
        teacherToChange.minAmountOfGradesToPassTheExam = editedDetails.minAmountOfGradesToPassTheExam


        setEditedDetails({
            name: "",
            meanGradeToPassExam: 0,
            minAmountOfGradesToPassTheExam: 0,

        })
        props.setIsEditDialogOpen(!props.isEditDialogOpen);
    }

    return <div>
        <Toaster>
            {props.error ? <Toast
                icon="notifications"
                intent="danger"
                timeout={500}
                message={props.error}></Toast> : null}

        </Toaster>
        <Dialog
            isOpen={props.isEditDialogOpen}
            title="Subject Details"
            onClose={() => props.setIsEditDialogOpen(!props.isEditDialogOpen)}
        >
            <div className={Classes.DIALOG_BODY}>


                <InputGroup
                    name="name"
                    value={editedDetails.name}
                    disabled={false}
                    leftIcon="book"
                    onChange={handleChange}
                    placeholder="Enter your name" />

                <InputGroup
                    name="meanGradeToPassExam"
                    value={String(editedDetails.meanGradeToPassExam)}
                    disabled={false}
                    leftIcon="user"
                    onChange={handleChange}
                    placeholder="Enter mean grade to pass the exam" />

                <InputGroup

                    name="minAmountOfGradesToPassTheExam"
                    value={String(editedDetails.minAmountOfGradesToPassTheExam)}
                    disabled={false}
                    leftIcon="envelope"
                    onChange={handleChange}
                    placeholder="Enter the min amount of grades to pass the exam" />





                <Button

                    icon="edit"
                    intent="success"
                    onClick={handleSubmit}
                // disabled={buttonDisabled()}
                >
                    {"Change"}
                </Button>
                <Button
                    icon="cross"
                    intent="danger"
                    onClick={() => props.setIsEditDialogOpen(!props.isEditDialogOpen)}
                // disabled={buttonDisabled()}
                >
                    {"Cancel"}
                </Button>




            </div>
        </Dialog >
    </div>
}
export default DialogEditInput