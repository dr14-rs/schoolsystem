import { Dialog, Classes, InputGroup, Button, Menu, HTMLSelect } from "@blueprintjs/core"
import { MenuItem2 } from "@blueprintjs/popover2";
import { ItemRendererProps, Select2 } from "@blueprintjs/select";
import React, { SyntheticEvent, useContext, useState } from "react"
import { SchoolContext } from "../../context/SchoolContext"
import { Subject } from "../../logic/subject"
import { Teacher } from "../../logic/teacher";

interface IProps {

    setError: (error: string | undefined) => void;
    isDialogOpen: boolean;
    setIsDialogOpen: (isDialogOpen: boolean) => void

}

const DialogInput: React.FunctionComponent<IProps> = (props) => {
    const school = useContext(SchoolContext)
    const subjects = school.subjects



    const [details, setDetails] = useState(
        {
            name: "",
            meanGradeToPassExam: 0,
            minAmountOfGradesToPassTheExam: 0,

        }
    )






    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {

        const { name, value } = e.target
        console.log(name, value)
        setDetails((prev) => {
            return { ...prev, [name]: value }
        })

    }

    const handleSubmit = (e: React.MouseEvent<HTMLElement>) => {

        const { name, meanGradeToPassExam, minAmountOfGradesToPassTheExam, } = details
        try {
            school.addSubject(new Subject(school, name, meanGradeToPassExam, minAmountOfGradesToPassTheExam),)
        }
        catch (err: any) {
            props.setError(err.message)

        }

        setDetails({
            name: "",
            meanGradeToPassExam: 0,
            minAmountOfGradesToPassTheExam: 0,

        })
        { props.setIsDialogOpen } (!props.isDialogOpen);

    }
    const buttonDisabled = () => {
        const { name, meanGradeToPassExam, minAmountOfGradesToPassTheExam } = details
        return (name && meanGradeToPassExam && minAmountOfGradesToPassTheExam) ? false : true

    }


    return <div>
        <Dialog
            isOpen={props.isDialogOpen}
            title="Subject Details"
            onClose={() => props.setIsDialogOpen(!props.isDialogOpen)}
        >
            <div className={Classes.DIALOG_BODY}>

                <InputGroup
                    name="name"
                    value={details.name}
                    disabled={false}
                    leftIcon="book"
                    onChange={handleChange}
                    placeholder="Enter subject's name" />

                <InputGroup
                    name="meanGradeToPassExam"
                    fill={true}
                    value={String(details.meanGradeToPassExam)}
                    disabled={false}
                    leftIcon="numerical"
                    onChange={handleChange}
                    placeholder="Enter mean grade to pass the exam" />

                {/* <InputGroup
                    name="minAmountOfGradesToPassTheExam"
                    value={String(details.minAmountOfGradesToPassTheExam)}
                    disabled={false}
                    leftIcon="numerical"
                    onChange={handleChange}
                    placeholder="Enter min amount of grades to pass the exam" /> */}


                < HTMLSelect
                    disabled={false}
                    fill={true}                >

                    <option>None </option>
                    {
                        school.teachers.map((teacher, index) =>
                            <option value={index} key={index}>{`
                        ${teacher.name}  ${teacher.surname} `}
                            </option>
                        )


                    }



                </HTMLSelect>

                <Button
                    icon="plus"
                    intent="danger"
                    onClick={handleSubmit}
                    disabled={buttonDisabled()}
                >
                    {"Click to add"}
                </Button>



            </div>
        </Dialog >
    </div>
}
export default DialogInput