import { Button, Classes, Dialog, HTMLTable, InputGroup, Toast, Toaster } from "@blueprintjs/core";
import { Popover2 } from "@blueprintjs/popover2";

import { observer } from "mobx-react";
import React, { useState } from "react";
import { useContext } from "react";
import { SchoolContext } from "../../context/SchoolContext";
import { Subject } from "../../logic/subject";
import DialogEditInput from "./DialogEditInput";
import DialogInput from "./DialogInput";
import StatisticDialog from "./StatisticDialog";







const Subjects: React.FunctionComponent = observer(({ }) => {
    const school = useContext(SchoolContext)
    const subjects = school.subjects

    const [isDialogOpen, setIsDialogOpen] = useState(false);
    const [isEditDialogOpen, setIsEditDialogOpen] = useState(false);
    const [editingSubject, setEditingSubject] = useState<Subject>()
    const [error, setError] = useState<string | undefined>(undefined)
    const [deletingSubject, setDeletingSubject] = useState<Subject>()
    const [isStatisticDialogOpen, setIsStatisticDialogOpen] = useState(false)
    const [subjectToCheck, setSubjectToCheck] = useState<Subject | undefined>()


    const handleEdit = (subject: Subject) => {

        setEditingSubject(subject)
        setIsEditDialogOpen(!isEditDialogOpen);

    }


    const handleDelete = (subject: Subject) => {
        setDeletingSubject(subject)
        console.log(subject.name)

        school.subjects = school.subjects.filter((f) => f.name !== subject.name)
        console.log(subjects)
    }
    const statisticTable = (subject: Subject) => {
        setIsStatisticDialogOpen(!isStatisticDialogOpen)
        setSubjectToCheck(subject)

    }

    return (
        <div >

            <Toaster>
                {error ? <Toast
                    icon="notifications"
                    intent="danger"
                    timeout={500}
                    message={error}></Toast> : null}

            </Toaster>
            <HTMLTable
                condensed={false}
                interactive={true}
                striped={true}

            >
                <thead>
                    <tr>
                        <th>Subject's name</th>
                        <th>Mean grade</th>
                        <th>Teacher</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    {subjects.map((subject, index) =>
                        <tr key={index}>
                            <td>{subject.name}</td>
                            <td>{subject.meanGradeToPassExam}</td>
                            <td>{subject.teacher?.name} {subject.teacher?.surname}</td>

                            <td>
                                <Button
                                    minimal={true}
                                    icon="edit"
                                    onClick={() => handleEdit(subject)} />
                                <Button
                                    minimal={true}
                                    icon="trash"
                                    intent="danger"
                                    onClick={() => handleDelete(subject)} />
                                <Button
                                    minimal={true}
                                    icon="list-columns"
                                    intent="success"
                                    onClick={() => statisticTable(subject)} />
                            </td>
                        </tr>)}

                </tbody>
            </HTMLTable>
            <DialogInput
                setError={setError}
                isDialogOpen={isDialogOpen}
                setIsDialogOpen={setIsDialogOpen} />
            <DialogEditInput
                error={error}
                setError={setError}
                isEditDialogOpen={isEditDialogOpen}
                setIsEditDialogOpen={setIsEditDialogOpen}
                editingSubject={editingSubject}
                setEditingSubject={setEditingSubject}
            />
            <StatisticDialog
                subjectToCheck={subjectToCheck}
                isStatisticDialogOpen={isStatisticDialogOpen}
                setIsStatisticDialogOpen={setIsStatisticDialogOpen}
            />

            <Button

                text="Add"
                intent="success"
                icon="plus"
                onClick={() => setIsDialogOpen(!isDialogOpen)}

            />






        </div >
    )
})


export default Subjects