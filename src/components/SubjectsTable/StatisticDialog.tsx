import { Dialog } from '@blueprintjs/core'
import { Cell, Column, Table2 } from '@blueprintjs/table'
import React, { useContext } from 'react'
import { SchoolContext } from '../../context/SchoolContext'
import { Subject } from '../../logic/subject'


interface IProps {
    isStatisticDialogOpen: boolean
    setIsStatisticDialogOpen: (isStatisticDialogOpen: boolean) => void
    subjectToCheck: Subject | undefined

}


const StatisticDialog: React.FunctionComponent<IProps> = (props) => {
    const school = useContext(SchoolContext)




    return <div>

        <Dialog
            isOpen={props.isStatisticDialogOpen}
            title="Subject Details"
            onClose={() => props.setIsStatisticDialogOpen(!props.isStatisticDialogOpen)}

            autoFocus={true}
            canEscapeKeyClose={true}
            canOutsideClickClose={true}
            enforceFocus={true}
            shouldReturnFocusOnClose={true}
            usePortal={true}>





            <Table2 numRows={school.students.length}>
                <Column
                    key="name"
                    name="Name"
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let studentNameRender = `${record.student.name}`;
                        return <Cell>{studentNameRender}</Cell>
                    }} />
                <Column
                    key="surname"
                    name="Surname"
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let studentSurnameRender = `${record.student.surname}`;
                        return <Cell>{studentSurnameRender}</Cell>
                    }} />
                <Column
                    key="email "
                    name="Email"
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let studentSurnameRender = `${record.student.email}`;
                        return <Cell>{studentSurnameRender}</Cell>
                    }} />
                <Column
                    key="meanGrade"
                    name="Mean Grade"
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let studentMeanGradeRender = `${record.student.getMeanGrade(props.subjectToCheck)}`;
                        return <Cell>{studentMeanGradeRender}</Cell>
                    }} />
                <Column
                    key="passedExam"
                    name="Passed Exam"
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let studentMeanGradeRender = `${record.student.examPassed(props.subjectToCheck!)}`;
                        return <Cell>{studentMeanGradeRender}</Cell>
                    }} />




            </Table2>

        </Dialog >
    </div>

}

export default StatisticDialog