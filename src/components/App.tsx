
import SchoolNavbar from "./SchoolNavbar"
import Journal from "./Journal"
import { SchoolContextProvider } from "../context/SchoolContext"





export const App = () => {
    return (<div>
        <SchoolContextProvider>

            <SchoolNavbar />
            <Journal />

        </SchoolContextProvider>
    </div>
    )
}
export default App