export * from "../logic/school";
export * from "../logic/teacher";
export * from "../logic/student";
export * from "../logic/subject";
