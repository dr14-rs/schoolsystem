import { Dialog, Classes, InputGroup, Button, Toaster, Toast } from "@blueprintjs/core"
import React, { useContext, useState } from "react"
import { SchoolContext } from "../../context/SchoolContext"
import { Student } from "../../logic/student";



interface IProps {
    setEditingStudent: (editingStudent: Student | undefined) => void;
    editingStudent: Student | undefined;

    error: string | undefined
    setError: (error: string | undefined) => void;

    isEditDialogOpen: boolean;
    setIsEditDialogOpen: (isEdiDialogOPen: boolean) => void

}

const DialogEditInput: React.FunctionComponent<IProps> = (props) => {
    const school = useContext(SchoolContext)
    const students = school.students


    const [editedDetails, setEditedDetails] = useState({
        name: props?.editingStudent?.name || "",
        surname: props.editingStudent?.surname || "",
        email: props.editingStudent?.email || "",
        dateOfBirth: props.editingStudent?.dateOfBirth || ""
    })

    React.useEffect(() => {
        setEditedDetails({
            name: props?.editingStudent?.name || "",
            surname: props.editingStudent?.surname || "",
            email: props.editingStudent?.email || "",
            dateOfBirth: props.editingStudent?.dateOfBirth || ""
        })
    }, [props.editingStudent])

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {

        const { name, value } = e.target
        console.log(name, value)
        setEditedDetails((prev) => {
            return { ...prev, [name]: value }
        })

    }
    const handleSubmit = (e: React.MouseEvent<HTMLElement>) => {
        const studentToChange = school.students.find((student) => student.name === props?.editingStudent?.name &&
            student.surname === props?.editingStudent.surname &&
            student.email === props.editingStudent.email &&
            student.dateOfBirth === props.editingStudent.dateOfBirth)!

        console.log(editedDetails)
        console.log(studentToChange)
        console.log(students)
        studentToChange.surname = editedDetails.surname
        studentToChange.name = editedDetails.name
        studentToChange.email = editedDetails.email
        studentToChange.dateOfBirth = editedDetails.dateOfBirth


        setEditedDetails({
            name: "",
            surname: "",
            email: "",
            dateOfBirth: "",
        })
        props.setIsEditDialogOpen(!props.isEditDialogOpen);
    }

    return <div>
        <Toaster>
            {props.error ? <Toast
                icon="notifications"
                intent="danger"
                timeout={500}
                message={props.error}></Toast> : null}

        </Toaster>
        <Dialog
            isOpen={props.isEditDialogOpen}
            title="Student Details"
            onClose={() => props.setIsEditDialogOpen(!props.isEditDialogOpen)}
        >
            <div className={Classes.DIALOG_BODY}>


                <InputGroup
                    name="name"
                    value={editedDetails.name}
                    disabled={false}
                    leftIcon="user"
                    onChange={handleChange}
                    placeholder="Enter your name" />

                <InputGroup
                    name="surname"
                    value={editedDetails.surname}
                    disabled={false}
                    leftIcon="user"
                    onChange={handleChange}
                    placeholder="Enter your surname" />

                <InputGroup

                    name="email"
                    value={editedDetails.email}
                    disabled={true}
                    leftIcon="envelope"
                    onChange={handleChange}
                    placeholder="Enter your email" />

                <InputGroup
                    name="dateOfBirth"
                    value={String(editedDetails.dateOfBirth)}
                    disabled={false}
                    leftIcon="numerical"
                    onChange={handleChange}
                    placeholder="Enter your date of birth" />



                <Button

                    icon="edit"
                    intent="success"
                    onClick={handleSubmit}
                // disabled={buttonDisabled()}
                >
                    {"Change"}
                </Button>
                <Button
                    icon="cross"
                    intent="danger"
                    onClick={() => props.setIsEditDialogOpen(!props.isEditDialogOpen)}
                // disabled={buttonDisabled()}
                >
                    {"Cancel"}
                </Button>




            </div>
        </Dialog >
    </div>
}
export default DialogEditInput