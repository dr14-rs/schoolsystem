import { Dialog } from '@blueprintjs/core'
import { Cell, Column, Table2 } from '@blueprintjs/table'
import React, { useContext } from 'react'
import { SchoolContext } from '../../context/SchoolContext'


interface IProps {
    isStatisticDialogOpen: boolean
    setIsStatisticDialogOpen: (isStatisticDialogOpen: boolean) => void

}


const StatisticDialog: React.FunctionComponent<IProps> = (props) => {
    const school = useContext(SchoolContext)




    return <div>

        <Dialog
            isOpen={props.isStatisticDialogOpen}
            title="Student Details"
            onClose={() => props.setIsStatisticDialogOpen(!props.isStatisticDialogOpen)}>



            <Table2 numRows={school.students.length}>

                <Column
                    key="name"
                    name="Name"
                    cellRenderer={index => {
                        let record = school.teacherSubjectJournal[index]
                        let teacherNameRender = `${record.teacher.name}`;
                        return <Cell>{teacherNameRender}</Cell>
                    }} />
                <Column
                    key="surname"
                    name="Surname"
                    cellRenderer={index => {
                        let record = school.teacherSubjectJournal[index]
                        let teacherSurnameRender = `${record.teacher.surname}`;
                        return <Cell>{teacherSurnameRender}</Cell>
                    }} />
                <Column
                    key="subjectName"
                    name="Email"
                    cellRenderer={index => {
                        let record = school.teacherSubjectJournal[index]
                        let emailRender = `${record.teacher.email}`;
                        console.log(record)
                        return <Cell>{emailRender}</Cell>
                    }} />




            </Table2>


        </Dialog >
    </div>

}

export default StatisticDialog