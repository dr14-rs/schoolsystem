import { Button, Classes, Dialog, HTMLTable, InputGroup, Toast, Toaster } from "@blueprintjs/core";
import { Popover2 } from "@blueprintjs/popover2";

import { observer } from "mobx-react";
import React, { useState } from "react";
import { useContext } from "react";
import { SchoolContext } from "../../context/SchoolContext";
import { Student } from "../../logic/student";
import DialogEditInput from "./DialogEditInput";

import DialogInput from "./DialogInput";
import StatisticDialog from "./StatisticDialog";




const Students: React.FunctionComponent = observer(() => {
    const school = useContext(SchoolContext)
    const students = school.students

    const [isDialogOpen, setIsDialogOpen] = useState(false);
    const [isEditDialogOpen, setIsEditDialogOpen] = useState(false);
    const [editingStudent, setEditingStudent] = useState<Student>()
    const [error, setError] = useState<string | undefined>(undefined)
    const [deletingStudent, setDeletingStudent] = useState<Student>()
    const [isStatisticDialogOpen, setIsStatisticDialogOpen] = useState(false)


    const handleEdit = (student: Student) => {

        setEditingStudent(student)
        setIsEditDialogOpen(!isEditDialogOpen);

    }


    const handleDelete = (student: Student) => {
        setDeletingStudent(student)
        console.log(student.email)

        school.students = school.students.filter((f) => f.email !== student.email)
        console.log(students)
    }
    const statisticTable = (student: Student) => {
        setIsStatisticDialogOpen(!isStatisticDialogOpen)
    }

    return (
        <div >

            <Toaster>
                {error ? <Toast
                    icon="notifications"
                    intent="danger"
                    timeout={500}
                    message={error}></Toast> : null}

            </Toaster>
            <HTMLTable
                condensed={false}
                interactive={true}
                striped={true}

            >
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Age</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    {students.map((student, index) =>
                        <tr key={index}>
                            <td>{student.name}</td>
                            <td>{student.surname} </td>
                            <td> {student.email} </td>
                            <td> {student.age} </td>
                            <td>
                                <Button
                                    minimal={true}
                                    icon="edit"
                                    onClick={() => handleEdit(student)} />
                                <Button
                                    minimal={true}
                                    icon="trash"
                                    intent="danger"
                                    onClick={() => handleDelete(student)} />
                                <Button
                                    minimal={true}
                                    icon="list-columns"
                                    intent="success"
                                    onClick={() => statisticTable(student)} />
                            </td>
                        </tr>)}

                </tbody>
            </HTMLTable>
            <DialogInput
                setError={setError}
                isDialogOpen={isDialogOpen}
                setIsDialogOpen={setIsDialogOpen} />
            <DialogEditInput
                error={error}
                setError={setError}
                isEditDialogOpen={isEditDialogOpen}
                setIsEditDialogOpen={setIsEditDialogOpen}
                editingStudent={editingStudent}
                setEditingStudent={setEditingStudent}
            />
            <StatisticDialog

                isStatisticDialogOpen={isStatisticDialogOpen}
                setIsStatisticDialogOpen={setIsStatisticDialogOpen}

            />
            <Button

                text="Add"
                intent="success"
                icon="plus"
                onClick={() => setIsDialogOpen(!isDialogOpen)}

            />






        </div >
    )
})


export default Students