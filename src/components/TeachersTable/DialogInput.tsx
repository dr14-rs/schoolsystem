import { Dialog, Classes, InputGroup, Button } from "@blueprintjs/core"
import React, { useContext, useState } from "react"
import { SchoolContext } from "../../context/SchoolContext"
import { Teacher } from "../../logic/teacher"

interface IProps {

    setError: (error: string | undefined) => void;
    isDialogOpen: boolean;
    setIsDialogOpen: (isDialogOpen: boolean) => void

}

const DialogInput: React.FunctionComponent<IProps> = (props) => {
    const school = useContext(SchoolContext)
    const teachers = school.teachers


    const [details, setDetails] = useState(
        {
            name: "",
            surname: "",
            email: "",
            dateOfBirth: "",

        }
    )


    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {

        const { name, value } = e.target
        console.log(name, value)
        setDetails((prev) => {
            return { ...prev, [name]: value }
        })

    }

    const handleSubmit = (e: React.MouseEvent<HTMLElement>) => {

        const { name, surname, email, dateOfBirth } = details
        try {
            school.addTeacher(new Teacher(name, surname, email, dateOfBirth, school))
        }
        catch (err: any) {
            props.setError(err.message)

        }

        setDetails({
            name: "",
            surname: "",
            email: "",
            dateOfBirth: "",
        })
        { props.setIsDialogOpen } (!props.isDialogOpen);

    }
    const buttonDisabled = () => {
        const { name, surname, email, dateOfBirth } = details
        return (name && surname && email && dateOfBirth) ? false : true

    }

    return <div>
        <Dialog
            isOpen={props.isDialogOpen}
            title="Teacher Details"
            onClose={() => props.setIsDialogOpen(!props.isDialogOpen)}
        >
            <div className={Classes.DIALOG_BODY}>

                <InputGroup
                    name="name"
                    value={details.name}
                    disabled={false}
                    leftIcon="user"
                    onChange={handleChange}
                    placeholder="Enter your name" />

                <InputGroup
                    name="surname"
                    value={details.surname}
                    disabled={false}
                    leftIcon="user"
                    onChange={handleChange}
                    placeholder="Enter your surname" />

                <InputGroup
                    name="email"
                    value={details.email}
                    disabled={false}
                    leftIcon="envelope"
                    onChange={handleChange}
                    placeholder="Enter your email" />

                <InputGroup
                    name="dateOfBirth"
                    value={details.dateOfBirth}
                    disabled={false}
                    leftIcon="numerical"
                    onChange={handleChange}
                    placeholder="Enter your date of birth" />



                <Button
                    icon="plus"
                    intent="danger"
                    onClick={handleSubmit}
                    disabled={buttonDisabled()}
                >
                    {"Click to add"}
                </Button>



            </div>
        </Dialog >
    </div>
}
export default DialogInput