import { Button, Classes, Dialog, HTMLTable, InputGroup, Toast, Toaster } from "@blueprintjs/core";
import { Popover2 } from "@blueprintjs/popover2";

import { observer } from "mobx-react";
import React, { useState } from "react";
import { useContext } from "react";
import { SchoolContext } from "../../context/SchoolContext";
import { Subject } from "../../logic/subject";
import { Teacher } from "../../logic/teacher";
import DialogEditInput from "./DialogEditInput";

import DialogInput from "./DialogInput";
import StatisticDialog from "./StatisticDialog";




const Teachers: React.FunctionComponent = observer(({ }) => {
    const school = useContext(SchoolContext)
    const teachers = school.teachers

    const [isDialogOpen, setIsDialogOpen] = useState(false);
    const [isEditDialogOpen, setIsEditDialogOpen] = useState(false);
    const [isStatisticDialogOpen, setIsStatisticDialogOpen] = useState(false)
    const [editingTeacher, setEditingTeacher] = useState<Teacher>()
    const [deletingTeacher, setDeletingTEacher] = useState<Teacher>()
    const [statisticForTeacher, setStatisticForTeacher] = useState<Teacher>()
    const [subjectTeacherTeaches, setSubjectTeacherTeaches] = useState<string | undefined>()

    const [error, setError] = useState<string | undefined>(undefined)


    const handleEdit = (teacher: Teacher) => {

        setEditingTeacher(teacher)
        setIsEditDialogOpen(!isEditDialogOpen);
    }


    const handleDelete = (teacher: Teacher) => {

        setDeletingTEacher(teacher)
        console.log(teacher.email)

        school.teachers = school.teachers.filter((f) => f.email !== teacher.email)
        console.log(teachers)
    }



    const statisticTable = (teacher: Teacher) => {
        setSubjectTeacherTeaches(school.subjectForTeacher(teacher)?.name)
        setStatisticForTeacher(teacher)
        setIsStatisticDialogOpen(!isStatisticDialogOpen)
    }

    return (
        <div >

            <Toaster>
                {error ? <Toast
                    icon="notifications"
                    intent="danger"
                    timeout={500}
                    message={error}></Toast> : null}

            </Toaster>
            <HTMLTable
                condensed={false}
                interactive={true}
                striped={true}

            >
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Surname</th>
                        <th>Email</th>
                        <th>Age</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                    {teachers.map((teacher, index) =>
                        <tr key={index}>
                            <td>{teacher.name}</td>
                            <td>{teacher.surname} </td>
                            <td> {teacher.email} </td>
                            <td> {teacher.age} </td>
                            <td>
                                <Button
                                    minimal={true}
                                    icon="edit"
                                    onClick={() => handleEdit(teacher)} />
                                <Button
                                    minimal={true}
                                    icon="trash"
                                    intent="danger"
                                    onClick={() => handleDelete(teacher)} />
                                <Button
                                    minimal={true}
                                    icon="list-columns"
                                    intent="success"
                                    onClick={() => statisticTable(teacher)}
                                />
                            </td>
                        </tr>)}

                </tbody>
            </HTMLTable>
            <DialogInput
                setError={setError}
                isDialogOpen={isDialogOpen}
                setIsDialogOpen={setIsDialogOpen} />
            <DialogEditInput
                error={error}
                setError={setError}
                isEditDialogOpen={isEditDialogOpen}
                setIsEditDialogOpen={setIsEditDialogOpen}
                editingTeacher={editingTeacher}
                setEditingTeacher={setEditingTeacher} />
            <StatisticDialog
                statisticForTeacher={statisticForTeacher}
                setStatisticForTeacher={setStatisticForTeacher}

                subjectTeacherTeaches={subjectTeacherTeaches}
                isStatisticDialogOpen={isStatisticDialogOpen}
                setIsStatisticDialogOpen={setIsStatisticDialogOpen}

            />
            <Button

                text="Add"
                intent="success"
                icon="plus"
                onClick={() => setIsDialogOpen(!isDialogOpen)}

            />






        </div >
    )
})


export default Teachers