import { Button, Dialog } from '@blueprintjs/core'
import { Cell, Column, Table2 } from '@blueprintjs/table'
import React, { useContext } from 'react'
import { SchoolContext } from '../../context/SchoolContext'
import { Teacher } from '../../logic/teacher'


interface IProps {
    statisticForTeacher: Teacher | undefined
    setStatisticForTeacher: (statisticForTeacher: Teacher | undefined) => void

    subjectTeacherTeaches: string | undefined

    isStatisticDialogOpen: boolean
    setIsStatisticDialogOpen: (isStatisticDialogOpen: boolean) => void

}


const StatisticDialog: React.FunctionComponent<IProps> = (props) => {
    const school = useContext(SchoolContext)




    return <div>

        <Dialog
            isOpen={props.isStatisticDialogOpen}
            title="Teacher Details"

            onClose={() => props.setIsStatisticDialogOpen(!props.isStatisticDialogOpen)}>



            <Table2 numRows={school.students.length}>
                <Column
                    key="name"
                    name="Name"
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let studentNameRender = `${record.student.name}`;
                        return <Cell>{studentNameRender}</Cell>
                    }} />
                <Column
                    key="surname"
                    name="Surname"
                    cellRenderer={index => {
                        let record = school.enrollmentJournal[index]
                        let studentSurnameRender = `${record.student.surname}`;
                        return <Cell>{studentSurnameRender}</Cell>
                    }} />

                <Column
                    key="grade"
                    name="Grades"
                    cellRenderer={index => {
                        let record = school.gradesForStudent(school.students[index])

                        let studentGradeRender = `${record.get(props.subjectTeacherTeaches!)}`;

                        return <Cell>{studentGradeRender}</Cell>
                    }}
                />


            </Table2>


        </Dialog >
    </div>

}

export default StatisticDialog