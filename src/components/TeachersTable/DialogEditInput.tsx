import { Dialog, Classes, InputGroup, Button, Toaster, Toast } from "@blueprintjs/core"
import React, { useContext, useState } from "react"
import { SchoolContext } from "../../context/SchoolContext"
import { Teacher } from "../../logic/teacher";


interface IProps {
    setEditingTeacher: (editingTeacher: Teacher | undefined) => void;
    editingTeacher: Teacher | undefined;

    error: string | undefined
    setError: (error: string | undefined) => void;

    isEditDialogOpen: boolean;
    setIsEditDialogOpen: (isEdiDialogOPen: boolean) => void

}

const DialogEditInput: React.FunctionComponent<IProps> = (props) => {
    const school = useContext(SchoolContext)
    const teachers = school.teachers


    const [editedDetails, setEditedDetails] = useState({
        name: props?.editingTeacher?.name || "",
        surname: props.editingTeacher?.surname || "",
        email: props.editingTeacher?.email || "",
        dateOfBirth: props.editingTeacher?.dateOfBirth || ""
    })

    React.useEffect(() => {
        setEditedDetails({
            name: props?.editingTeacher?.name || "",
            surname: props.editingTeacher?.surname || "",
            email: props.editingTeacher?.email || "",
            dateOfBirth: props.editingTeacher?.dateOfBirth || ""
        })
    }, [props.editingTeacher])

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {

        const { name, value } = e.target
        console.log(name, value)
        setEditedDetails((prev) => {
            return { ...prev, [name]: value }
        })

    }
    const handleSubmit = (e: React.MouseEvent<HTMLElement>) => {
        const teacherToChange = school.teachers.find((teacher) => teacher.name === props?.editingTeacher?.name &&
            teacher.surname === props?.editingTeacher.surname &&
            teacher.email === props.editingTeacher.email &&
            teacher.dateOfBirth === props.editingTeacher.dateOfBirth)!

        console.log(editedDetails)
        console.log(teacherToChange)
        console.log(teachers)
        teacherToChange.surname = editedDetails.surname
        teacherToChange.name = editedDetails.name
        teacherToChange.email = editedDetails.email
        teacherToChange.dateOfBirth = editedDetails.dateOfBirth


        setEditedDetails({
            name: "",
            surname: "",
            email: "",
            dateOfBirth: "",
        })
        props.setIsEditDialogOpen(!props.isEditDialogOpen);
    }

    return <div>
        <Toaster>
            {props.error ? <Toast
                icon="notifications"
                intent="danger"
                timeout={500}
                message={props.error}></Toast> : null}

        </Toaster>
        <Dialog
            isOpen={props.isEditDialogOpen}
            title="Teacher Details"
            onClose={() => props.setIsEditDialogOpen(!props.isEditDialogOpen)}
        >
            <div className={Classes.DIALOG_BODY}>


                <InputGroup
                    name="name"
                    value={editedDetails.name}
                    disabled={false}
                    leftIcon="user"
                    onChange={handleChange}
                    placeholder="Enter your name" />

                <InputGroup
                    name="surname"
                    value={editedDetails.surname}
                    disabled={false}
                    leftIcon="user"
                    onChange={handleChange}
                    placeholder="Enter your surname" />

                <InputGroup

                    name="email"
                    value={editedDetails.email}
                    disabled={true}
                    leftIcon="envelope"
                    onChange={handleChange}
                    placeholder="Enter your email" />

                <InputGroup
                    name="dateOfBirth"
                    value={String(editedDetails.dateOfBirth)}
                    disabled={false}
                    leftIcon="numerical"
                    onChange={handleChange}
                    placeholder="Enter your date of birth" />



                <Button

                    icon="edit"
                    intent="success"
                    onClick={handleSubmit}
                // disabled={buttonDisabled()}
                >
                    {"Change"}
                </Button>
                <Button
                    icon="cross"
                    intent="danger"
                    onClick={() => props.setIsEditDialogOpen(!props.isEditDialogOpen)}
                // disabled={buttonDisabled()}
                >
                    {"Cancel"}
                </Button>




            </div>
        </Dialog >
    </div>
}
export default DialogEditInput