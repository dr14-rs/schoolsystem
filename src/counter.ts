import { action, autorun, observable } from "mobx";

interface ICounter {
	item: number;
}

export class Counter {
	@observable count: number = 1;

	@action.bound
	increment(): void {
		this.count = this.count + 1;
	}
}

// const newCounter = new Counter();

// autorun(() => {
// 	newCounter.items.length;
// 	console.log("Number of items : " + newCounter.items.length);
// });
// newCounter.addNumber(3);
// newCounter.addNumber(4);
