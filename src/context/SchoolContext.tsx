import React from "react";
import { createContext, ReactNode } from "react";
import { School } from "../logic/school";


// type SchoolContextProviderProps = { children: ReactNode }

// export const SchoolContext = createContext(new School())

// export const SchoolContextProvider = ({ children }: SchoolContextProviderProps) => {
//     const school = new School();
//     school.addTeacher(new Teacher("Mathematical", "Genious", "Mathematical.Genious@teacher.com", "1978-04-12", school),)
//     school.addTeacher(new Teacher("Biological", "Wizzard", "Biological.Wizzard@teacher.com", "1987-12-30", school),)
//     school.addTeacher(new Teacher("Language", "Speakius", "Language.Speakius@teacher.com", "1986-07-02", school),)



//     return <SchoolContext.Provider value={school}>{children}</SchoolContext.Provider>

// }







export const SchoolContext = createContext(new School());
export function useSchoolContext() {
    const context = React.useContext(SchoolContext);
    if (context === undefined) {
        throw new Error('useSchoolContext must be used within a SchoolContextProvider');
    }
    return context;
}

export const SchoolContextProvider: React.FunctionComponent<{ school?: School, children: React.ReactNode }> = ({
    school,
    children
}) => {
    return (
        <SchoolContext.Provider value={school || new School()}>
            {children}
        </SchoolContext.Provider>
    );
};